# Desarrollo Web en entorno servidor. Grupo DAW2
## Entrega
- Toma fotos de la pantalla cada 5s con Chronollapse. 
- Déjalas en tu carpeta de red.
- Guarda tu código en bitbucket y compártelo con el profesor.
## Enunciado
Haz un FORK en el repositorio  git@bitbucket.org:rafacabeza/examen1703.git .  En él ya cuentas con:
1. La estructura base de Laravel 
2. Migración de las tablas y seeders. 
3. FALTA: modelos y métodos que reflejen las relaciones de clave ajena entre ellas. 
###  Desarrollar un proyecto de una biblioteca. Con las siguientes tablas:
1. User (ya definida por Laravel). 
2. Gender. Género de libros
3. Book. Libros.
4. Author. Autor.  
5. Relaciones: Gender-Book (1:N), Author-Book (N:M).
### Ejercicios
1. User (ya definida por Laravel). 
2. Gender. Género de libros
3. Book. Libros.
4. Author. Autor.  
5. Relaciones: Gender-Book (1:N), Author-Book (N:M).
Ejercicios
1. Configura Laravel para comenzar a funcionar. Crea una base de datos examen1701. 
2. Añade los modelos necesarios y sus métodos de relaciones para que las migraciones y seeders funcionen correctamente. (1Punto) 
3. Crea un controlador resource para gestionar la tabla de libros (books) y la ruta o rutas necesarias. 
4. Index. Listado de libros. Debe incluír el nombre del usuario que lo da de alta y el nombre de género. (1 Punto) 
5. create/store. Alta de libros. El usuario es el logueado. El género debe elegirse en un control select. (1.5 Punto). 
6. Validación del alta de libros con mensajes en español. (1 Punto). 
7. show. Muestra del detalle de un libro. Debe incluír la lista de autores. (1 Punto). 
8. delete. Borrado de libros. (1 Punto). 
9. Autorización (1.5 Puntos). 
 - INDEX/SHOW. Listado y vista de detalle. Acceso público. 
 - CREATE/STORE. Creación de libros. Acceso para todos los usuarios logueados. Además el enlace de nuevo en la lista de libros debe mostrarse/ocultarse según corresponda. 
 - DELETE. Borrado de libros. Sólo puede hacerlo el usuario propietario. El botón de borrado sólo debe aparecer para los libros del usuario actual. 
10. API REST básica del modelo Author. Crea el controlador correspondiente y la ruta o rutas necesarias en api.php. Debe contener los métodos index, show, store, delete y update. Debes comprobarlos con Postman (2 Puntos).